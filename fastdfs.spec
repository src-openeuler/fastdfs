Name: fastdfs
Version: 6.12.0
Release: 2
Summary: FastDFS server and client
License: GPL3.0
Group: Arch/Tech
URL: 	https://github.com/happyfish100/fastdfs
Source: https://github.com/happyfish100/fastdfs/archive/V%{version}.tar.gz

Patch1:        0001-remove-compile-warnings-in-client-and-storage.patch

Requires: gcc cmake
BuildRequires: libfastcommon-devel >= 1.0.66
BuildRequires: libserverframe-devel >= 1.2.1
BuildRequires: perl

%description
This package provides tracker & storage of fastdfs

%package -n fastdfs-server
Requires: libfastcommon >= 1.0.66
Requires: libserverframe >= 1.2.1
Requires: fastdfs-config
Summary: fastdfs tracker & storage

%package -n fastdfs-tools
Requires: libfdfsclient
Summary: fastdfs tools

%package -n libfdfsclient
Requires: libfastcommon >= 1.0.66
Requires: libserverframe >= 1.2.1
Requires: fastdfs-config
Summary: The client dynamic library of fastdfs

%package -n libfdfsclient-devel
Requires: libfdfsclient
Summary: The client header of fastdfs

%description -n fastdfs-server
This package provides tracker & storage of fastdfs

%description -n libfdfsclient
This package is client dynamic library of fastdfs

%description -n libfdfsclient-devel
This package is client header of fastdfs client

%package -n fastdfs-config
Summary: FastDFS config files for sample

%description -n fastdfs-tools
This package is tools for fastdfs

%description -n fastdfs-config
FastDFS config files for sample
commit version: $COMMIT_VERSION

%prep
%autosetup -n %{name}-%{version} -p1

%build
./make.sh clean && ./make.sh

%install
rm -rf %{buildroot}
DESTDIR=$RPM_BUILD_ROOT ./make.sh install

%post

%postun

%clean
rm -rf %{buildroot}

%files

%files -n fastdfs-server
%defattr(-,root,root,-)
%{_bindir}/fdfs_trackerd
%{_bindir}/fdfs_storaged
%config(noreplace) /usr/lib/systemd/system/fdfs_trackerd.service
%config(noreplace) /usr/lib/systemd/system/fdfs_storaged.service

%files -n libfdfsclient
%defattr(-,root,root,-)
%{_libdir}/libfdfsclient*
%{_usr}/lib/libfdfsclient*

%files -n libfdfsclient-devel
%defattr(-,root,root,-)
%{_includedir}/fastdfs/*

%files -n fastdfs-tools
%defattr(-,root,root,-)
%{_bindir}/fdfs_monitor
%{_bindir}/fdfs_test
%{_bindir}/fdfs_test1
%{_bindir}/fdfs_crc32
%{_bindir}/fdfs_upload_file
%{_bindir}/fdfs_download_file
%{_bindir}/fdfs_delete_file
%{_bindir}/fdfs_file_info
%{_bindir}/fdfs_appender_test
%{_bindir}/fdfs_appender_test1
%{_bindir}/fdfs_append_file
%{_bindir}/fdfs_upload_appender
%{_bindir}/fdfs_regenerate_filename

%files -n fastdfs-config
%defattr(-,root,root,-)
%config(noreplace) /etc/fdfs/*.conf

%changelog
* Fri Aug 2 2024 zhangyaqi <zhangyaqi@kylinos.cn> - 6.12.0-2
- remove compile warnings

* Mon Feb 26 2024 liweigang <izmirvii@gmail.com> - 6.12.0-1
- update to version 6.12.0

* Mon Mar 20 2023 suweifeng <suweifeng@huawei.com> - 6.9.4-1
- Upgrade to version 6.9.4

* Fri Dec 16 2022 lihaoxiang <lihaoxiang9@huawei.com> - 6.9.1-1
- Upgrade to version 6.9.1

* Thu Jun 23 2022 lihaoxiang <lihaoxiang9@huawei.com> - 6.06-3
- Modified fastdfs.spec that add BuildRequires about perl

* Fri Apr 02 2021 herengui <herengui@uniontech.com> - 6.06-2
- Rearrange the files list

* Fri Sep 11 2020 huyan90325 <huyan90325@talkweb.com.cn> - 6.06-1
- Fastdfs package init
